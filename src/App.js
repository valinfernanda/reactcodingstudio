import React, { useState } from "react";
import { Button } from "@material-ui/core";

import DeleteIcon from "@material-ui/icons/Delete";
import TextField from "@material-ui/core/TextField";
import "./App.css";

const data = [
  { message: "Cari makan", id: 1 },
  { message: "Belajar React", id: 2 },
  { message: "Belajar Lainnya", id: 3 },
];

const Header = () => {
  return <h1>Aplikasi Taskku </h1>;
};

const InfoBar = ({ taskNumber }) => {
  return <div>Ada {taskNumber} yang perlu dikerjakan </div>;
};

const TaskAdder = ({ setTasks, tasks }) => {
  const [currentValue, setCurrentValue] = useState("");

  const handleAddTask = () => {
    const newTask = {
      id: tasks.length + 1,
      message: currentValue,
    };
    setTasks([...tasks, newTask]);
    setCurrentValue("");
  };
  return (
    <div className="task-adder">
      <TextField
        type="text"
        value={currentValue}
        onChange={(event) => setCurrentValue(event.target.value)}
        variant="outlined"
        label="Tambah task"
      />
      <Button
        variant="contained"
        color="primary"
        disabled={currentValue === ""}
        onClick={() => handleAddTask()}
      >
        Tambah
      </Button>
    </div>
  );
};

const Task = ({ message, id, setTasks, tasks }) => {
  const handleDelete = () => {
    const updatedTasks = tasks.filter((task) => task.id !== id);
    setTasks(updatedTasks);
  };
  return (
    <div className="container-task">
      <div className="task">
        <div style={{ display: "inline-block" }}>{message}</div>
        <Button
          variant="contained"
          color="secondary"
          startIcon={<DeleteIcon />}
          onClick={() => handleDelete()}
        >
          Delete
        </Button>
      </div>
    </div>
  );
};

const TaskList = ({ tasks, setTasks }) => {
  return tasks.map((task) => {
    return (
      <Task
        message={task.message}
        id={task.id}
        setTasks={setTasks}
        tasks={tasks}
      />
    );
  });
};
const TaskApp = () => {
  const [tasks, setTasks] = useState(data);
  return (
    <div className="container">
      <p>
        <Header />
        <InfoBar taskNumber={tasks.length} />
        {/* pake kurawal karna mau passing nilai integer */}
        <TaskAdder setTasks={setTasks} tasks={tasks} />
        <TaskList tasks={tasks} setTasks={setTasks} />
      </p>
    </div>
  );
};

const App = () => {
  return <TaskApp />;
};

export default App;
